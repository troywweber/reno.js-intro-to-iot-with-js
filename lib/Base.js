module.exports = class Base
{
    constructor(board, pin)
    {
        if (!board) throw 'board is undefined';
        if (typeof pin !== 'string' && typeof pin !== 'number') throw 'pin should be a number (or string such as A0)';

        // give extending objects board access
        this.__board = board;

        // establish the pin whether digital or analog
        this.__pin = typeof pin === 'string' ? parseInt(pin.substring(1)) : pin;
        if (!board.pins[this.__pin]) throw `board does not have pin ${typeof pin === 'string' ? pin : `D${pin}`}`;

        // establish a name for logging
        this.name = `${this.constructor.name} ${typeof pin === 'string' ? pin : `D${pin}`}`;
        this.__log = (... args) => console.log(`[${this.name}]`, ... args);
    }
};
