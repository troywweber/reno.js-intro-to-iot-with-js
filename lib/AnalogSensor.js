module.exports = class AnalogSensor extends require('./Base')
{
    constructor(board, pin)
    {
        super(board, pin);

        this.__reporting = false;
        this.handler = (value) => this.__log('value', value);

        this.__log('created');
    }

    set handler(callback)
    {
        this.__board.removeAllListeners('analog-read-' + this.__pin);
        this.__board.analogRead(this.__pin, callback);
        this.Report(this.__reporting);
        this.__log('new handler set');
    }

    get reporting ()
    {
        return this.__reporting;
    }

    Report(setReporting)
    {
        this.__reporting = setReporting;
        this.__board.reportAnalogPin(this.__pin, setReporting ? 1 : 0);
        this.__log('reporting is', setReporting ? 'on' : 'off');
    }
};
