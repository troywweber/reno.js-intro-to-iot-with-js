module.exports = class Switch extends require('./Base')
{
    constructor(board, pin)
    {
        super(board, pin);
        board.pinMode(this.__pin, board.MODES.OUTPUT);
        this.Off();
        this.__log('created');
    }

    On()
    {
        this.__state = this.__board.HIGH;
        this.__board.digitalWrite(this.__pin, this.__state);
        this.__log('On');
    }

    Off()
    {
        this.__state = this.__board.LOW;
        this.__board.digitalWrite(this.__pin, this.__state);
        this.__log('Off');
    }

    Toggle()
    {
        this.__state === this.__board.HIGH ? this.Off() : this.On();
    }
};
