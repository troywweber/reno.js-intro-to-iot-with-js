#!/usr/bin/env node

const EPC = require('etherport-client').EtherPortClient,
	Board = require('firmata').Board,
	repl = require('repl'),
	host = process.argv[2];

let port = process.argv[3];

console.log(`script given host '${host}' and port ${port}`);

if (!host || isNaN(port = parseInt(port)))
{
	console.log('host and port number required. i.e script.js [host] [port]');
	process.exit(1);
}

// create the tcp serialport client, specifying the host and port on which to connect

let etherport = new EPC({ host, port }),
	board = global.board = new Board(etherport);

board.once('ready', () =>
{
	console.log('connected to board over wifi!');
	board.setSamplingInterval(500);
	repl.start();
});

console.log('board may take several seconds to connect ...');
