#!/usr/bin/env node

const EPC = require('etherport-client').EtherPortClient,
	Board = require('firmata').Board,
	repl = require('repl'),
	host = process.argv[2];

let port = process.argv[3];

console.log(`script given host '${host}' and port ${port}`);

if (!host || isNaN(port = parseInt(port)))
{
	console.log('host and port number required. i.e script.js [host] [port]');
	process.exit(1);
}

// set up globally accessible classes

const AnalogSensor = global.AnalogSensor = require('../lib/AnalogSensor'),
    Switch = global.Switch = require('../lib/Switch');

console.log('Switch & AnalogSensor classes will be available in REPL');

// create the tcp serialport client, specifying the host and port on which to connect

let etherport = new EPC({ host, port }),
	board = global.board = new Board(etherport);

// once the board is ready, set up all the objects on it

board.once('ready', () =>
{
	console.log('connected to board over wifi!');
	board.setSamplingInterval(500);

	let components = [
		{
			name: 'Wifi_LED',
			obj: new Switch(board, 9)
		},
		{
            name: 'Fan',
            obj: new Switch(board, 2)
		},
        {
            name: 'PhotoResistor',
            obj: new AnalogSensor(board, 'A0')
        }
	];

	for (let component of components)
	{
		global[component.name] = component.obj;
		console.log(`'${component.obj.name}' available in REPL as '${component.name}'`);
	}

	// quick demo logic!

	global.FanConnect = () =>
	{
		let sensor = global['PhotoResistor'],
			fan = global['Fan'];

		console.log('connecting fan to photo resistor, call PhotoResistor.Report(true)');

		sensor.handler = (value) =>
		{
			if (sensor.reporting) value > 400 ? fan.On() : fan.Off();
		};
	};

	global.FanDisconnect = () =>
	{
        let sensor = global['PhotoResistor'],
            fan = global['Fan'];

        console.log('disconnecting fan');

        sensor.handler = (value) => sensor.__log('got value', value);
        fan.Off();
	};

	repl.start();
});

console.log('board may take several seconds to connect ...');
