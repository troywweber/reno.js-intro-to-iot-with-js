# README

This project is intended for use with the ["Intro to IoT with JS" Reno.js talk][1] given on May 9, 2017.
To understand more about the content of this repository,
[check out the slideshow from the talk][2].

### Who do I talk to?

* You can [Email][3] **Troy Weber**, Mechatronics / Software Developer

[1]: https://www.meetup.com/RenoJS/events/238500433/
[2]: https://docs.google.com/presentation/d/1yDbAG6YyTdjpLnXcXUcPVsjqYeiHdgykANQArbM8mCI/pub?start=false&loop=false&delayms=30000
[3]: mailto:troywweber.public@mailnull.com
